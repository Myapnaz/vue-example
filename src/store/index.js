import Vue from "vue";
import Vuex from "vuex";
//import axios from "axios";

Vue.use(Vuex);

import { urun } from './modules/Urun.module';
import { siparis } from './modules/Siparis.module';

export default new Vuex.Store({
  modules: {
    urun,
    siparis,
  }
});


























// export default new Vuex.Store({
//   state: {
//     urunler: [],
//     siparisler: [],
//     filtreTanimlar: []
//   },
//   mutations: {
//     getUrunList(state, urunler){
//       state.urunler = urunler;
//     },
//     getSiparisler(state,siparisler){
//       state.siparisler = siparisler;
//     },
//     getFiltreTanimlar(state,tanim){
//       state.filtreTanimlar = tanim;
//     }
//   },
//   actions: {
//     UrunleriGetir({commit}, filtreler){    
//       axios.post('http://192.168.1.57/vue_api/api/values/GetUrunList',filtreler)
//       .then(response => {
//         commit('getUrunList', response.data);
//       });
//     },
//     SiparisleriGetir({commit}){
//       axios.post("http://192.168.1.57/vue_api/api/values/GetSiparisList")
//       .then(response => {
//         commit('getSiparisler', response.data);
//       });
//     },
//     FiltreTanimlariGetir({commit}){
//       axios.post("http://192.168.1.57/vue_api/api/values/GetUrunTanimList")
//       .then(response => {
//         commit('getFiltreTanimlar', response.data);
//       });
//     }
//   },
//   modules: {}
// });
