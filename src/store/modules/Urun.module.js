import axios from "axios";

const getDefaultState = () => {
    return {
        urunler: [],
        filtreTanimlar: [],
    };
  };
  
  export const urun = {
    namespaced: true,
    state: getDefaultState(),
    getters: {},
    mutations: {
        getUrunList(state, urunler){
          state.urunler = urunler;
        },
        getFiltreTanimlar(state,tanim){
          state.filtreTanimlar = tanim;
        }
    },
    actions: {

        UrunleriGetir({commit}, filtreler){    
            axios.post('http://192.168.1.57/vue_api/api/values/GetUrunList',filtreler)
            .then(response => {
              commit('getUrunList', response.data);
            });
          },

        FiltreTanimlariGetir({commit}){
            axios.post("http://192.168.1.57/vue_api/api/values/GetUrunTanimList")
            .then(response => {
              commit('getFiltreTanimlar', response.data);
            });
        },
    }
  };
  