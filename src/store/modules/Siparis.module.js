import axios from "axios";

const getDefaultState = () => {
    return {
        siparisler: []
    };
  };
  
  export const siparis = {
    namespaced: true,
    state: getDefaultState(),
    getters: {
      getKg5 : state => {
        return state.siparisler.filter(s => s.SIPARIS_KG > 5)
      }
    },
    mutations: {
        getSiparisler(state,siparisler){
            state.siparisler = siparisler;
          },
    },
    actions: {
        SiparisleriGetir({commit}){
          return axios.post("http://192.168.1.57/vue_api/api/values/GetSiparisList")
            .then(response => {
              commit('getSiparisler', response.data);
            });
          },
    }
  };
  