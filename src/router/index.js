import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/urun_listesi",
    name: "urun_listesi,",
    component: () => import("../views/Urun_listesi.vue")
  },
  {
    path: "/siparis_listesi",
    name: "siparis_listesi,",
    component: () => import("../views/Siparis_listesi.vue")
  },
  {
    path: "/urun_form/:id?",
    name: "urun_form",
    component: () => import("../views/Urun_form.vue")
  },
  {
    path: "/siparis_form/:id?",
    name: "siparis_form",
    component: () => import("../views/Siparis_form.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  routes
});

export default router;
